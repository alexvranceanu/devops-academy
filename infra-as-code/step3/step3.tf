provider "aws" {
  region     = "eu-west-1"
  access_key = "AKIAIYQ7TZD3DQCSFTWA"
  secret_key = "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"
}

# Step 2 - add SSH key details
resource "aws_key_pair" "auth" {
  key_name_prefix = "terraform"
  public_key = "${file("terraform.pub")}"

}

resource "aws_instance" "master" {
  ami           = "ami-061b1560"
  instance_type = "t2.small"
  # Step 2 - add connection details and startup-command
  connection {
    # The default username for our AMI
    user = "centos"
    private_key = "${file("terraform.key")}"
  }
  key_name = "${aws_key_pair.auth.id}"

  provisioner "remote-exec" {
    inline = [
      "sudo yum -y install epel-release python git",
      "sudo yum -y install ansible",
      "git clone https://alexvranceanu@gitlab.com/alexvranceanu/devops-academy.git && cd devops-academy && git checkout develop && cd ansible",
      "cat <<HERE > hosts",
      "[all]",
      "master ansible_ssh_host=${aws_instance.master.private_dns}",
      "minion1 ansible_ssh_host=${aws_instance.minion1.private_dns}",
      "minion2 ansible_ssh_host=${aws_instance.minion2.private_dns}",
      "HERE",
      "cp -p ~/devops-academy/infra-as-code/step3/terraform.key ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa",
      "git remote set-url origin git@gitlab.com:alexvranceanu/devops-academy.git"
    ]
  }


}

resource "aws_instance" "minion1" {
  ami           = "ami-061b1560"
  instance_type = "t2.micro"
  # Step 2 - add connection details and startup-command
  connection {
    # The default username for our AMI
    user = "centos"
    private_key = "${file("terraform.key")}"
  }
  key_name = "${aws_key_pair.auth.id}"
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y install python git"
    ]
  }

}

resource "aws_instance" "minion2" {
  ami           = "ami-061b1560"
  instance_type = "t2.micro"
  # Step 2 - add connection details and startup-command
  connection {
    # The default username for our AMI
    user = "centos"
    private_key = "${file("terraform.key")}"
  }
  key_name = "${aws_key_pair.auth.id}"
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y install python git"
    ]
  }

}
